/**
   Project: microservice-demo-table-list
      Test: smoke-test
    Author: Mitch Allen
*/

"use strict";

var request = require('supertest'),
    should = require('should'),
    verbose = process.env.TEST_VERBOSE || false,
    port = process.env.TABLE_LIST_PORT || 8001,
    testServer = process.env.TEST_SERVER || 'localhost',
    testHost = "http://" + testServer + ":" + port,
    testTable = process.env.TEST_TABLE || 'Music';

describe('demo table list smoke test', function() {
    var server = null;
    beforeEach(function() {
        // Needed for cleanup between tests
        var modulePath = '../index';
        delete require.cache[require.resolve(modulePath)];
        var obj = require(modulePath);  // Start GET service
        server = obj.server;
    });

    afterEach(function(done) {
        server.close(done);            // Stop GET service
    });

    it('should get a successful response from the server', function(done) {
        var testUrl =  "/admin/table/list";
        request(testHost)
            .get(testUrl)
            .expect(200)
            .end(function (err, res){
                should.not.exist(err);
                should.exist(res.body);
                should.exist(res.body.TableNames);
                should.exist(res.body.TableNames[0]);
                res.body.TableNames[0].should.eql(testTable);
                done();
            });
    });
});