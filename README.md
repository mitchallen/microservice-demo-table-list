microservice-demo-table-list
============================

Demo creating microservices
------------------------------------------------

## Work in Progress

This document is a work in progress.

## Requirements

This demo requires a local copy of __Amazon DynamoDB__ to be running on port 8000.

In order to see results the database should contain at least one table.

## Demo

In a terminal window type: 

    node index.js

In a second terminal window type (on one line):

    curl -i -X GET -H "Content-Type: application/json" http://localhost:8001/admin/table/list

Or in Chrome, browse to: 

    http://localhost:8001/admin/table/list

## Testing

To run the tests:

    npm test

* * *

## Version History

#### Version 1.0.2 release notes

* Updated dependencies

#### Version 1.0.1 release notes

* Added README
* Updated dependencies

#### Version 1.0.0 release notes

* Initial release